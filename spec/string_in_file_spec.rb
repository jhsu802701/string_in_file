require 'spec_helper'
file_to_write = File.expand_path('tmp_file_to_write.txt', __dir__)

RSpec.describe StringInFile do
  it 'has a version number' do
    expect(StringInFile::VERSION).not_to be nil
  end

  it 'does something useful' do
    expect(true).to eq(true)
  end

  it 'should return the contents of a file' do
    file_to_read = File.expand_path('file_to_read.txt', __dir__)
    string_to_read = StringInFile.read(file_to_read)
    expect(string_to_read).to eq('Doppler Value Investing / Buffett')
    expect(StringInFile.present('Doppler', file_to_read)).to eq(true)
    expect(StringInFile.present('Conventional', file_to_read)).to eq(false)
    expect(StringInFile.present('Doppler', '/etc/doesnotexist.txt')).to eq(false)
  end

  it 'should write a string to a file' do
    string_to_write = 'Bargain Stock Funds/'
    StringInFile.write(string_to_write, file_to_write)
    string_to_read = StringInFile.read(file_to_write)
    expect(string_to_read).to eq(string_to_write)
    expect(StringInFile.present('Bargain', file_to_write)).to eq(true)
    expect(StringInFile.present('Bond', file_to_write)).to eq(false)
    expect(StringInFile.present('Bargain', '/etc/doesnotexist.txt')).to eq(false)
  end

  it 'should replace a string in a file' do
    string_to_write = 'Bargain Stock Funds/'
    StringInFile.write(string_to_write, file_to_write)
    string1 = 'Stock'
    string2 = 'Equity'
    StringInFile.replace(string1, string2, file_to_write)
    string_to_read = StringInFile.read(file_to_write)
    expect(string_to_read).to eq('Bargain Equity Funds/')
    StringInFile.replace('/', '', file_to_write)
    string_to_read = StringInFile.read(file_to_write)
    expect(string_to_read).to eq('Bargain Equity Funds')
  end

  it 'attempt to replace a non-existent string in a file leaves it unchanged' do
    string_to_write = 'Bargain Stock Funds'
    StringInFile.write(string_to_write, file_to_write)
    string1 = 'Cheap'
    string2 = 'Cheapest'
    StringInFile.replace(string1, string2, file_to_write)
    string_to_read = StringInFile.read(file_to_write)
    expect(string_to_read).to eq('Bargain Stock Funds')
  end

  it 'should add a string to the beginning of a file' do
    string_to_write = "four five six\n"
    StringInFile.write(string_to_write, file_to_write)
    StringInFile.add_beginning("one two three\n", file_to_write)
    array_lines = IO.readlines(file_to_write)
    expect(array_lines[0]).to eq("one two three\n")
    expect(array_lines[1]).to eq("four five six\n")
  end

  it 'should add a string to the end of a file' do
    string_to_write = "one two three\n"
    StringInFile.write(string_to_write, file_to_write)
    StringInFile.add_end("four five six\n", file_to_write)
    array_lines = IO.readlines(file_to_write)
    expect(array_lines[0]).to eq("one two three\n")
    expect(array_lines[1]).to eq("four five six\n")
  end
end
