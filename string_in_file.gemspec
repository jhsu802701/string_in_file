lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'string_in_file/version'

Gem::Specification.new do |spec|
  spec.name          = 'string_in_file'
  spec.version       = StringInFile::VERSION
  spec.authors       = ['Jason Hsu']
  spec.email         = ['rubyist@jasonhsu.com']

  spec.summary       = 'Write string values to files and read file contents into strings.'
  spec.description   = 'Write string values to files and read file contents into strings.'
  spec.homepage      = 'https://bitbucket.org/jhsu802701/string_in_file'
  spec.license       = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '1.16.3'
  spec.add_development_dependency 'rake', '12.3.1'
  spec.add_development_dependency 'rspec', '3.8.0'

  spec.add_development_dependency 'bundler-audit', '0.6.0'
  spec.add_development_dependency 'gemsurance', '0.9.0'
  spec.add_development_dependency 'rubocop', '0.58.2'
  spec.add_development_dependency 'ruby-graphviz', '1.2.3'
  spec.add_development_dependency 'simplecov', '0.16.1'

  spec.add_development_dependency 'codecov', '0.1.10'
end
