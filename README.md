# StringInFile

[![Gem Version](https://badge.fury.io/rb/string_in_file.svg)](https://badge.fury.io/rb/string_in_file)
[![Build Status](https://semaphoreci.com/api/v1/jhsu802701/string_in_file/branches/master/badge.svg)](https://semaphoreci.com/jhsu802701/string_in_file)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/a819811f15d4406ab94cbd1d50c5a11a)](https://www.codacy.com/project/jhsu802701/string_in_file/dashboard?utm_source=jhsu802701@bitbucket.org&amp;utm_medium=referral&amp;utm_content=jhsu802701/string_in_file&amp;utm_campaign=Badge_Grade_Dashboard)
[![codebeat badge](https://codebeat.co/badges/b20c8e08-a6f7-46fd-9222-e6536b23277e)](https://codebeat.co/projects/bitbucket-org-jhsu802701-string_in_file-master)
[![codecov](https://codecov.io/bb/jhsu802701/string_in_file/branch/master/graph/badge.svg)](https://codecov.io/bb/jhsu802701/string_in_file)

This gem allows you to store a string value in a file. It's the ultimate global variable.  This gem can also be used to replace a string in a file with another string, determine if a string is present in a file, or add a string to the beginning or end of a file.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'string_in_file'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install string_in_file

## Usage

* Store the string "Doppler Value Investing" at /home/wbuffett/test.txt:
```
StringInFile.write('Doppler Value Investing', '/home/wbuffett/test.txt')
```

* Read the string stored at /home/wbuffett/test.txt:
```
StringInFile.read('/home/wbuffett/test.txt')
```

* Replace "Doppler" with "Next Generation" in the string stored at /home/wbuffett/test.txt:
```
StringInFile.replace('Doppler', 'Next Generation', '/home/wbuffett/test.txt')
```

* Check to see if the string "Doppler" is present at /home/wbuffett/test.txt:
```
StringInFile.present('Doppler', '/home/wbuffett/test.txt')
```

* Add a string to the beginning of a file:
```
StringInFile.add_beginning("one two three\n", 'numbers.txt')
```

* Add a string to the end of a file
```
StringInFile.add_end("four five six\n", 'numbers.txt')
```

## Development

* To test this gem, enter the command "sh gem_test.sh".
* To install this gem, enter the command "sh gem_install.sh".
* To test the source code for various metrics, enter the command "sh code_test.sh".
* To do all of the above, enter the command "sh all.sh".
* To run an interactive prompt, enter the command "sh console.sh".
* To release a new version, update the version number in the lib/(name of gem)/version.rb file, and then run "bundle exec rake release".  This creates a git tag for the version, push git commits and tags, and pushes the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on BitBucket at https://bitbucket.org/jhsu802701/string_in_file. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the StringInFile project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/jhsu802701/string_in_file/src/master/CODE_OF_CONDUCT.md).
